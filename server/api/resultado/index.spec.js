'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var resultadoCtrlStub = {
  index: 'resultadoCtrl.index',
  show: 'resultadoCtrl.show',
  create: 'resultadoCtrl.create',
  upsert: 'resultadoCtrl.upsert',
  patch: 'resultadoCtrl.patch',
  destroy: 'resultadoCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var resultadoIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './resultado.controller': resultadoCtrlStub
});

describe('Resultado API Router:', function() {
  it('should return an express router instance', function() {
    resultadoIndex.should.equal(routerStub);
  });

  describe('GET /api/resultados', function() {
    it('should route to resultado.controller.index', function() {
      routerStub.get
        .withArgs('/', 'resultadoCtrl.index')
        .should.have.been.calledOnce;
    });
  });

  describe('GET /api/resultados/:id', function() {
    it('should route to resultado.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'resultadoCtrl.show')
        .should.have.been.calledOnce;
    });
  });

  describe('POST /api/resultados', function() {
    it('should route to resultado.controller.create', function() {
      routerStub.post
        .withArgs('/', 'resultadoCtrl.create')
        .should.have.been.calledOnce;
    });
  });

  describe('PUT /api/resultados/:id', function() {
    it('should route to resultado.controller.upsert', function() {
      routerStub.put
        .withArgs('/:id', 'resultadoCtrl.upsert')
        .should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/resultados/:id', function() {
    it('should route to resultado.controller.patch', function() {
      routerStub.patch
        .withArgs('/:id', 'resultadoCtrl.patch')
        .should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/resultados/:id', function() {
    it('should route to resultado.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'resultadoCtrl.destroy')
        .should.have.been.calledOnce;
    });
  });
});
