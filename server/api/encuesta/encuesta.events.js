/**
 * Encuesta model events
 */

'use strict';

import {EventEmitter} from 'events';
var EncuestaEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
EncuestaEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Encuesta) {
  for(var e in events) {
    let event = events[e];
    Encuesta.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    EncuestaEvents.emit(event + ':' + doc._id, doc);
    EncuestaEvents.emit(event, doc);
  };
}

export {registerEvents};
export default EncuestaEvents;
