'use strict';

import angular from 'angular';

export default angular.module('acosoEnLineaApp.constants', [])
  .constant('appConfig', require('../../constant-config'))
  .name;
