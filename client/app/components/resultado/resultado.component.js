'use strict';
import angular from 'angular';
import echarts from 'echarts';
const $ = require('jquery');

let colorList = ['#00AC9B', '#FFE153', '#ED3F5F', '#007CB4'];
export class resultadoComponent {
  constructor($scope, $stateParams, $http, $state, $window, scrollTo, appConfig) {
    'ngInject';
    this.$scope = $scope;
    this.$stateParams = $stateParams;
    this.$http = $http;
    this.$state = $state;
    this.$window = $window;
    this.scrollTo = scrollTo;
    this.general = this.$stateParams.general;
    this.title;
    this.openAccordion = {};

    this.preguntasRespuestas = this.$window.localStorage.getItem('respuestas');
    if(this.preguntasRespuestas) {
      this.preguntasRespuestas = JSON.parse(this.preguntasRespuestas);
    }
  }

  $onInit() {
    this.chart = {
      bar: echarts.init(document.getElementById('bar'))
    };
    this.resultIndiv = angular.fromJson(this.$window.localStorage.getItem('resultInd'));
    if (this.general) {
      this.getResultadoGeneral();
    } else {
      this.result = this.$stateParams.result;
      this.$window.localStorage.setItem('resultInd',angular.toJson(this.result));
      this.title = "Resultados <u>individuales</u><br> por categoría";
      this.barChart();
    }

    $(this.$window).resize(() => {
      if (this.chart.bar) {
        this.chart.bar.resize();
      }
    });
  }

  barChart(){
    var option = {
      xAxis: {
          type: 'value',
          show: false
      },
      grid: {
        top: 40,
        bottom: 40,
        left: 30,
        right: 40,
        containLabel: true
      },
      yAxis: {
          type: 'category',
          data: ['Acoso', 'Discurso de odio', 'Vigilancia'],
          axisLabel: {
              fontSize: 12
          },
          axisLine: {
              show: false
          },
          axisTick: {
              show: false
          }
      },
      series: [{
          data: [
              {
                  value: Math.round(this.result.cat1),
                  itemStyle: {color: colorList[0]},
              },
              {
                  value: Math.round(this.result.cat2),
                  itemStyle: {color: colorList[1]},
              },
              {
                  value: Math.round(this.result.cat3),
                  itemStyle: {color: colorList[2]},
              }
          ],
          type: 'bar',
          label: {
              normal: {
                  position: 'right',
                  show: true,
                  fontSize: 14,
                  distance: 3,
                  formatter: "{c}%"
              }
          },
      }]
    };

    this.chart.bar.on('click', (params) => {
      if (params.name){
        if (params.name === "Vigilancia"){
          this.openAccordion = {vigilancia: true};
          this.scrollTo('#vigilancia');
        }else if (params.name === "Discurso de odio"){
          this.openAccordion = {discurso_de_odio: true};
          this.scrollTo('#discurso-de-odio');
        }else if (params.name === "Acoso"){
          this.openAccordion = {acoso: true};
          this.scrollTo('#acoso');
        }
        this.$scope.$apply();
      }
    });

    this.chart.bar.setOption(option)
  }

  goToResulGen() {
    this.$state.go('main.resultado', {general: true}, {reload: true});
  }

  goToResulInd() {
    this.$state.go('main.resultado', {general: false, result: this.resultIndiv}, {reload: true});
  }

  getResultadoGeneral() {
    this.$http.get('/api/resultados')
      .then(response => {
        this.result = response.data;
        this.title = "Resultados <u>generales</u><br> por categoría";
        this.barChart();
      });
  }

  getPreguntaClass(preguntaId, selectedClass = 'panel-info') {
    if(!this.preguntasRespuestas) {
      return 'panel-default';
    }
    let respuesta = this.preguntasRespuestas[preguntaId-1].respuesta;
    if(respuesta === 'SI') {
      return selectedClass;      
    }
    return 'panel-default';
  }
}

export default angular.module('acosoEnLineaApp.resultado', [])
  .component('resultado', {
    template: require('./resultado.html'),
    controller: resultadoComponent,
    controllerAs: 'vm'
  })
  .run(function ($templateCache) {
    'ngInject';
    $templateCache.put('accordion-info-general.html', require('./accordion-html/accordion-info-general.html'));
    $templateCache.put('accordion-vigilancia.html', require('./accordion-html/accordion-vigilancia.html'));
    $templateCache.put('accordion-discurso-de-odio.html', require('./accordion-html/accordion-discurso-de-odio.html'));
    $templateCache.put('accordion-acoso.html', require('./accordion-html/accordion-acoso.html'));
    $templateCache.put('accordion-glosario.html', require('./accordion-html/accordion-glosario.html'));
    $templateCache.put('accordion-plataformas.html', require('./accordion-html/accordion-plataformas.html'));
    $templateCache.put('accordion-organizaciones.html', require('./accordion-html/accordion-organizaciones.html'));
    $templateCache.put('accordion-iniciativas-estatales.html', require('./accordion-html/accordion-iniciativas-estatales.html'));
  })
  .name;
