'use strict';
const angular = require('angular');

export class portadaComponent {
  constructor($state, $window, $http) {
    'ngInject';
    this.$state = $state;
    this.$window = $window;
    this.$http = $http;
    this.checkCaptcha = null;
  }

  goTo(path) {
    this.$state.go(path, {}, { reload: true });
  }

  $onInit() {
    this.$window.localStorage.removeItem('resultInd');
    grecaptcha.ready(() => {
      grecaptcha.execute('6LcbcKoUAAAAANtBw_bQ0KNIe-W4ki5Fm-5xXogX',
        { action: 'homepage' }).then((token) => {
          this.$http.post('/api/encuestas/captcha', {
            token: token
          }).then(response => {
            this.checkCaptcha = response.data;
          });;
        });
    });
  }

  sentTokenCaptcha(token) {
    this.$http.post('/api/encuestas/captcha', {
      token: token
    });
  }
}

export default angular.module('acosoEnLineaApp.portada', [])
  .component('portada', {
    template: require('./portada.html'),
    controller: portadaComponent,
    controllerAs: 'vm'
  })
  .name;


