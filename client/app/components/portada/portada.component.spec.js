'use strict';

describe('Component: portada', function() {
  // load the component's module
  beforeEach(module('acosoEnLineaApp.principal'));

  var principalComponent;

  // Initialize the component and a mock scope
  beforeEach(inject(function($componentController) {
    principalComponent = $componentController('principal', {});
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
