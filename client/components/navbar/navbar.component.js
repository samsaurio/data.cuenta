'use strict';
/* eslint no-sync: 0 */

import angular from 'angular';
import $ from 'jquery';

const navbarHeigth = 60; // px

export class NavbarComponent {
  constructor() {}
}

export default angular.module('directives.navbar', [])
  .component('navbar', {
    template: require('./navbar.html'),
    controller: NavbarComponent
  })
  .directive('scrollTo', function(scrollTo) {
    'ngInject';
    return {
      restrict: 'A',
      link(scope, element, attr) {
        let $el = $(element);
        let handler = () => {
          scrollTo(attr.scrollTo);
        };
        $el.bind('click', handler);
        scope.$on('$destroy', () => {
          $el.unbind('click', handler);
        });
      }
    };
  })
  .factory('scrollTo', function() {
    'ngInject';
    return function (scrollTo) {
      let scrollTop;
      if(scrollTo === 'top') {
        scrollTop = 0;
      } else {
        scrollTop = $(scrollTo).offset().top - navbarHeigth;
      }
      $('html, body').animate({
        scrollTop
      }, 1000);
    };
  })
  .name;
