# data.cuenta

Esta es una plataforma interactiva que pretende visibilizar, medir y conocer las experiencias de violencia que han sido mediadas por la tecnología. Desde la selfie controladora de tus sueños hasta esos mensajitos indeseados de gente indeseable.

Queremos saber qué te ha pasado, y queremos brindarte consejos y soluciones para hacerle frente a las formas de violencia que muchas veces no reconocemos en nuestro cotidiano.

# Desarrollo

Esta herramienta ha sido desarrollada en conjunto con [WebLab](https://www.weblab.com.py).

This project was generated with the [Angular Full-Stack Generator](https://github.com/DaftMonk/generator-angular-fullstack) version 4.2.3.

## Getting Started

### Prerequisites

- [Git](https://git-scm.com/)
- [Node.js and npm](nodejs.org) Node >= 4.x.x, npm >= 2.x.x
- [Gulp](http://gulpjs.com/) (`npm install --global gulp`)
- [MongoDB](https://www.mongodb.org/) - Keep a running daemon with `mongod`

### Developing

1. Run `npm install` to install server dependencies.

2. Run `mongod` in a separate shell to keep an instance of the MongoDB Daemon running

3. Run `gulp serve` to start the development server. It should automatically open the client in your browser when ready.

## Build & development

Run `gulp build` for building and `gulp serve` for preview.

## Testing

Running `npm test` will run the unit tests with karma.

## With docker

**Dependences:** You need `docker` and `docker-compose` installed

Build and up (first time):
`docker-compose up -d`

If you change something, then build, down and up again:
`docker-compose build && docker-compose down && docker-compose up -d`

**Config file:** check `server/config/environment/development.js` and change db connection from `mongodb://localhost/acosoenlinea-dev` to `mongodb://db/acosoenlinea-dev`.

### Production

Para hacer correr en modo producción, son necesarios los siguientes cambios
 - `docker-compose.yml`: reemplazar puertos `3000` por `8080`
 - `Dockerfile`: reemplazar `gulp serve` por `gulp serve:dist`

## Backup
El proceso para obtener un backup de la base de datos consiste en:

1. Ingresar al contenedor docker
2. Generar backup
3. Descargar archivos

### Ingresar al contenedor docker
Para ingresar al docker ejecutando el bash de forma interactiva:

`$ docker exec -it <container_id> bash`

para obtener el `container_id` puede ejecutar `$ docker ps` y buscar el `CONTAINER ID` para la `IMAGE` de la aplicación `acoso-en-linea_app`.

### Generar backup
Para generar los archivos CSV, ejecutar:

`$ npm run backup`

Esto generará los archivos CSV en la carpeta `./exported-data/<timestamp>` en donde estarán los archivos `encuesta.csv` (que contiene los datos de las respuestas individuales) y `resultados.csv` (que contiene los resultados generales).

### Descargar archivos
Para descargar los archivos generados puede utilizar SCP, por ejemplo:

`$ scp your_username@datacuenta.tedic.org:exported-data/<timestamp>/encuesta.csv /some/local/directory`